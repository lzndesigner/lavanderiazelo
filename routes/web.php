<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Front\HomeController@index');
Route::get('/campanha/cortina.html', 'Front\CampanhasController@cortina');
Route::get('/campanha/persiana.html', 'Front\CampanhasController@persiana');
Route::get('/campanha/sofa.html', 'Front\CampanhasController@sofa');
Route::get('/campanha/tapete.html', 'Front\CampanhasController@tapete');
Route::get('/campanha/cortina', 'Front\CampanhasController@cortina');
Route::get('/campanha/persiana', 'Front\CampanhasController@persiana');
Route::get('/campanha/sofa', 'Front\CampanhasController@sofa');
Route::get('/campanha/tapete', 'Front\CampanhasController@tapete');
Route::post('/orcar', 'Front\HomeController@sendmail')->name('orcar.sendmail');
