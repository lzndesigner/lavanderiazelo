<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <base href="{{ Request::url() }}" />
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Lavanderia Zelo | Lavagem de Sofás, carpetes, Cortinas, Tapetes, Persianas e Impermeabilização de estofados
    </title>
    <meta name="description"
        content="Atendemos toda São Paulo e grande São Paulo, não cobramos por retirada e entrega. Temos mais de 32 anos de tradição. Clique e venha conhecer nossa lavanderia. Melhor preço de São Paulo.">
    <meta name="robots" content="index, follow" />
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:type" content="website" />
    <meta property="og:title"
        content="Lavanderia Zelo | Atendemos toda São Paulo e grande São Paulo, não cobramos por retirada e entrega. Temos mais de 32 anos de tradição. Clique e venha conhecer nossa lavanderia. Melhor preço de São Paulo." />
    <meta property="og:description"
        content="Atendemos toda São Paulo e grande São Paulo, não cobramos por entrega e temos mais de 32 anos de tradição. Clique e venha conhecer nossa lavanderia. Melhor preço de São Paulo." />
    <meta property="og:url" content="https://limpezadesofas.com.br/" />
    <meta property="og:site_name" content="Lavanderia Zelo" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description"
        content="Atendemos toda São Paulo e grande São Paulo, não cobramos por retirada e entrega. Temos mais de 32 anos de tradição. Clique e venha conhecer nossa lavanderia. Melhor preço de São Paulo." />
    <meta name="twitter:title"
        content="Lavanderia Zelo | Atendemos toda São Paulo e grande São Paulo, não cobramos por retirada e entrega. Temos mais de 32 anos de tradição. Clique e venha conhecer nossa lavanderia. Melhor preço de São Paulo." />
    <link href="{{ asset('/galerias/favicon.ico?1') }}" rel="icon">
    <link rel="stylesheet" href="{{ asset('/minify/css/all.css?24') }}">
    <link href="{{ asset('/frontend/css/fonts.css?4') }}" rel="preload" as="style">
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NP9V6TR');

    </script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NP9V6TR" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="wrapper">

        <!-- =========================
        Header
    =========================== -->
        <header id="header" class="header header-light header-layout1">
            <div class="subHeader">
                <div class="container">
                    <div class="d-flex align-content-center justify-content-start">
                        <div class="">
                            <ul class="list-inline m-0">
                                <li><i class="fa fa-map-marker"></i> R. Baquirivu, 454 - Cidade Ademar - SP</li>
                                <li><i class="fa fa-envelope"></i> <a
                                        href="mailto:contato@lavanderiazelo.com.br">contato@lavanderiazelo.com.br</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4">
                        <a class="logo" href="{{ url('/') }}">
                            <img src="{{ asset('/galerias/logo_site.webp') }}" class="logo-dark"
                                alt="Lavanderia Zelo" width="280" height="100%">
                        </a>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="d-none d-sm-flex justify-content-center align-items-center headerContact">
                            <div class="flex-fill text-sm-left text-md-center text-lg-center text-xl-center mx-2">
                                <i class="fa fa-phone"></i>
                                <h4>Entre em Contato</h4>
                                <h3><a href="https://api.whatsapp.com/send?phone=5511984683174&text=Ola%20gostaria%20de%20fazer%20um%20or%C3%A7amento.%20Te%20encontrei%20no%20site.%20Muito%20obrigado"
                                        id="whatsapp" target="_Blank">(11) 9.8468-3174</a></h3>
                            </div>
                            <div class="flex-fill text-sm-left text-md-center text-lg-center text-xl-center mx-2">
                                <i class="fa fa-clock"></i>
                                <h4>SEG-SEX 08:00 ~ 19:00</h4>
                                <h4>SABÁDO 08:00 ~ 14:00</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <nav class="navbar navbar-expand-lg">
                <button class="navbar-toggler" type="button">
                    <span class="menu-lines"><span></span></span>
                </button>
                <div class="container-fluid px-0">
                    <div class="collapse navbar-collapse" id="mainNavigation">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav__item ">
                                <a href="#" id="headerlink" class="nav__item-link active">Início</a>
                            </li><!-- /.nav-item -->
                            <li class="nav__item">
                                <a href="#" id="servicoslink" class="nav__item-link">Serviços</a>
                            </li><!-- /.nav-item -->
                            <li class="nav__item">
                                <a href="https://api.whatsapp.com/send?phone=5511984683174&text=Ola%20gostaria%20de%20fazer%20um%20or%C3%A7amento.%20Te%20encontrei%20no%20site.%20Muito%20obrigado"
                                target="_Blank" class="nav__item-link">Orçamento</a>
                            </li><!-- /.nav-item -->
                            <li class="nav__item">
                                <a href="#" id="locallink" class="nav__item-link">Local</a>
                            </li><!-- /.nav-item -->
                            <li class="nav__item">
                                <a href="#" id="orcamentolink" class="nav__item-link">Contato</a>
                            </li><!-- /.nav-item -->
                        </ul><!-- /.navbar-nav -->
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container -->
            </nav><!-- /.navabr -->

        </header><!-- /.Header -->

        <!-- ============================
        Slider
    ============================== -->
        <section class="slider slider-layout1">
            <div class="slick-carousel carousel-arrows-light m-slides-0"
                data-slick='{"slidesToShow": 1, "arrows": true, "dots": true, "speed": 700,"fade": true,"cssEase": "linear"}'>
                <div class="slide-item align-v-h bg-overlay">
                    <div class="bg-img"><img src="{{ asset('/galerias/slider/estofados.webp?3') }}" alt="Estofados">
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="slide-item__content">
                                    <h1 class="slide-item__title">Lavagem de Estofados</h1>
                                    <p class="slide-item__desc">Com nosso sistema de higienização, recuperamos e
                                        revigorando as cores e a maciez original, proporcionando bem estar ao ambiente
                                        mais utilizado pela família.</p>
                                    <a href="https://api.whatsapp.com/send?phone=5511984683174&text=Ola%20gostaria%20de%20fazer%20um%20or%C3%A7amento.%20Te%20encontrei%20no%20site.%20Muito%20obrigado"
                                target="_Blank" class="btn btn__primary btn__lg mr-30">
                                        <i class="fa fa-arrow-right"></i><span> Faça seu Orçamento</span>
                                    </a>
                                </div><!-- /.slide-content -->
                            </div><!-- /.col-xl-9 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.slide-item -->
                <div class="slide-item align-v-h bg-overlay">
                    <div class="bg-img"><img src="{{ asset('/galerias/slider/tapetes.webp?3') }}" alt="Tapetes"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="slide-item__content">
                                    <h1 class="slide-item__title">Lavagem de Tapetes</h1>
                                    <p class="slide-item__desc">Lavamos todos os tipos de tapetes, para cada tipo, um
                                        tratamento especial. <br> Fazemos limpeza à seco e lavagem convencional.</p>
                                    <a href="https://api.whatsapp.com/send?phone=5511984683174&text=Ola%20gostaria%20de%20fazer%20um%20or%C3%A7amento.%20Te%20encontrei%20no%20site.%20Muito%20obrigado"
                                target="_Blank" class="btn btn__primary btn__lg mr-30">
                                        <i class="fa fa-arrow-right"></i><span> Faça seu Orçamento</span>
                                    </a>
                                </div><!-- /.slide-content -->
                            </div><!-- /.col-xl-9 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.slide-item -->
                <div class="slide-item align-v-h bg-overlay">
                    <div class="bg-img"><img src="{{ asset('/galerias/slider/impermeabilizacao.webp?3') }}"
                            alt="Imperabilização"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="slide-item__content">
                                    <h1 class="slide-item__title">Imperabilização</h1>
                                    <p class="slide-item__desc">Os produtos que utilizamos no processo de
                                        impermeabilização de estofados <br> não são inflamáveis e proporciona maior
                                        segurança para os clientes e nossos colaboradores.</p>
                                    <a href="https://api.whatsapp.com/send?phone=5511984683174&text=Ola%20gostaria%20de%20fazer%20um%20or%C3%A7amento.%20Te%20encontrei%20no%20site.%20Muito%20obrigado"
                                target="_Blank" class="btn btn__primary btn__lg mr-30">
                                        <i class="fa fa-arrow-right"></i><span> Faça seu Orçamento</span>
                                    </a>
                                </div><!-- /.slide-content -->
                            </div><!-- /.col-xl-9 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.slide-item -->
                <div class="slide-item align-v-h bg-overlay">
                    <div class="bg-img"><img src="{{ asset('/galerias/slider/persianas.webp?3') }}" alt="Persianas">
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="slide-item__content">
                                    <h1 class="slide-item__title">Lavagem de Persianas</h1>
                                    <p class="slide-item__desc">Resultados inéditos com limpeza completa de áreas
                                        porosas ou inacessíveis da persiana. <br> Capacidade para lavar persianas de
                                        grandes tamanhos.</p>
                                    <a href="https://api.whatsapp.com/send?phone=5511984683174&text=Ola%20gostaria%20de%20fazer%20um%20or%C3%A7amento.%20Te%20encontrei%20no%20site.%20Muito%20obrigado"
                                target="_Blank" class="btn btn__primary btn__lg mr-30">
                                        <i class="fa fa-arrow-right"></i><span> Faça seu Orçamento</span>
                                    </a>
                                </div><!-- /.slide-content -->
                            </div><!-- /.col-xl-9 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.slide-item -->
            </div><!-- /.carousel -->
        </section><!-- /.slider -->


        <!-- ========================
        Services Layout 2
    =========================== -->
        <section class="services-layout2 pb-90" id="servicos">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                        <h2 class="heading__subtitle color-thema">melhores soluções para lavagem de estofados</h2>
                        <h2 class="heading__title color-thema mb-50">Residência ou Empresas</h2>
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="slick-carousel"
                            data-slick='{"slidesToShow": 3, "slidesToScroll": 2, "arrows": true, "dots": true, "responsive": [ {"breakpoint": 992, "settings": {"slidesToShow": 2}}, {"breakpoint": 767, "settings": {"slidesToShow": 2}}, {"breakpoint": 480, "settings": {"slidesToShow": 1}}]}'>
                            <div class="service-item">
                                <div class="service-item__content">
                                    <div class="service-item__icon">
                                        <i class="fa fa-couch"></i>
                                    </div><!-- /.service-item__icon -->
                                    <h4 class="service-item__title">Lavagem de Estofados</h4>
                                    <p class="service-item__desc">Procedimento de lavagem para obter o máximo de
                                        resultado, tratamento de manchas especificas no estofado como manchas de urina,
                                        café, caneta, leite, doces e outros tipos que podem prejudicar o estofado.</p>
                                    <ul class="list-items list-items-layout2 list-unstyled mb-30">
                                        <li>Sofás, poltronas, almofadas,</li>
                                        <li>Cadeiras, cadeiras de escritório,</li>
                                        <li>Bancos, colchões e futtons</li>
                                    </ul>
                                    <a href="#" class="d-none btn btn__secondary">
                                        <i class="fa fa-arrow-right"></i>
                                        <span>Conheça mais</span>
                                    </a>
                                </div>
                            </div><!-- /.service-item -->

                            <div class="service-item">
                                <div class="service-item__content">
                                    <div class="service-item__icon">
                                        <i class="fa fa-star"></i>
                                    </div><!-- /.service-item__icon -->
                                    <h4 class="service-item__title">Lavagem de Tapetes</h4>
                                    <p class="service-item__desc">Capacidade para lavar tapetes de grande proporções com
                                        centrifugas para secagem rápida e uniforme removendo todo resido e sem danificar
                                        o tecido, todos são tratados como verdadeiras obras de artes.</p>
                                    <ul class="list-items list-items-layout2 list-unstyled mb-30">
                                        <li>Persa, Chinês, Kilim,</li>
                                        <li>Kilim, Iraniano</li>
                                        <li>e entre outros</li>
                                    </ul>
                                    <a href="#" class="d-none btn btn__secondary">
                                        <i class="fa fa-arrow-right"></i>
                                        <span>Conheça mais</span>
                                    </a>
                                </div>
                            </div><!-- /.service-item -->

                            <div class="service-item">
                                <div class="service-item__content">
                                    <div class="service-item__icon">
                                        <i class="fa fa-align-justify"></i>
                                    </div><!-- /.service-item__icon -->
                                    <h4 class="service-item__title">Lavagem de Persianas</h4>
                                    <p class="service-item__desc">A lavagem das persianas é feita através de métodos e
                                        produtos especiais de forma a não danificar as persianas no que refere-se é
                                        pintura, textura, formato e impermeabilização.</p>
                                    <ul class="list-items list-items-layout2 list-unstyled mt-60 mb-30">
                                        <li>Secagem em ambiente natural</li>
                                        <li>Qualidade de Serviço em Lavagem</li>
                                    </ul>
                                    <a href="#" class="d-none btn btn__secondary">
                                        <i class="fa fa-arrow-right"></i>
                                        <span>Conheça mais</span>
                                    </a>
                                </div>
                            </div><!-- /.service-item -->

                            <div class="service-item">
                                <div class="service-item__content">
                                    <div class="service-item__icon">
                                        <i class="fa fa-heart"></i>
                                    </div><!-- /.service-item__icon -->
                                    <h4 class="service-item__title">Lavagem de Carpetes</h4>
                                    <p class="service-item__desc">Eliminação de ácaros e bactérias e especifico para uma
                                        perfeita higienização. Extração de alta performance da sujeira e resíduos.
                                        Tratamento para lavagem de manchas especiais como manchas de urina animal no
                                        carpete.</p>
                                    <ul class="list-items list-items-layout2 list-unstyled mt-50 mb-50">
                                        <li>Equipamentos modernos</li>
                                    </ul>
                                    <a href="#" class="d-none btn btn__secondary">
                                        <i class="fa fa-arrow-right"></i>
                                        <span>Conheça mais</span>
                                    </a>
                                </div>
                            </div><!-- /.service-item -->

                            <div class="service-item">
                                <div class="service-item__content">
                                    <div class="service-item__icon">
                                        <i class="fa fa-tint"></i>
                                    </div><!-- /.service-item__icon -->
                                    <h4 class="service-item__title">Impermeabilização</h4>
                                    <p class="service-item__desc">Os estofados são impermeabilizados com ZONYL 2.000 da
                                        Dupont, Cappax da Cappa Comercial e Teximper água da Tex Imper Ltda, todos os
                                        produtos não inflamáveis, a base de água, que repelem líquidos, óleos, sujeiras
                                        secas e úmidas.</p>
                                    <ul class="list-items list-items-layout2 list-unstyled mt-30 mb-40">
                                        <li>Conservação do Tecido</li>
                                        <li>Evita Manchas</li>
                                    </ul>
                                    <a href="#" class="d-none btn btn__secondary">
                                        <i class="fa fa-arrow-right"></i>
                                        <span>Conheça mais</span>
                                    </a>
                                </div>
                            </div><!-- /.service-item -->
                        </div><!-- /.col-lg-12 -->
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.Services Layout 2 -->


        <!-- =========================
       Banner layout 4
      =========================== -->
        <section class="banner-layout4 bg-overlay bg-parallax" id="orcamento">
            <div class="bg-img"><img src="{{ asset('/galerias/limpeza-estofados.webp') }}" alt="background"></div>
        </section><!-- /.Banner layout 4 -->

        <!-- ==========================
        contact layout 2
      =========================== -->
        <section class="contact-layout2 py-0 bg-gray">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="contact-panel mt--150">
                            <div class="testimonials testimonials-layout1 bg-overlay bg-overlay-theme">
                                <div class="bg-img">
                                    <img src="{{ asset('/frontend/images/banners/6.webp') }}" alt="banner">
                                </div>
                                <div class="slick-carousel"
                                    data-slick='{"slidesToShow": 1, "arrows": true, "dots": false, "infinite": true}'>
                                    <!-- Testimonial #1 -->
                                    <div class="testimonial-item">
                                        <div class="testimonial-item__rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <p class="testimonial-item__desc">Desde 1984 a Lavanderia Zelo proporciona a
                                            seus clientes um serviço de extrema qualidade na lavagem de sofás, tapetes,
                                            carpetes, cortinas, persianas, alem de impermeabilização e restauração de
                                            tapetes.
                                        </p>
                                        <div class="testimonial-item__meta d-none">
                                            <div class="testimonial-item__thumb">
                                                <img src="{{ asset('/galerias/avatares/1.webp') }}"
                                                    alt="Martin Hope">
                                            </div><!-- /.testimonial-thumb -->
                                            <div>
                                                <h4 class="testimonial-item__meta-title">Martin Hope</h4>
                                                <p class="testimonial-item__meta-desc">Pro Cons</p>
                                            </div>
                                        </div><!-- /.testimonial-meta -->
                                    </div><!-- /. testimonial-item -->

                                </div>
                            </div><!-- /.testimonials-layout1 -->
                            <div class="contact-panel__form">
                                <form method="post" action="{{ route('orcar.sendmail') }}" id="orcamento_enviado">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h4 class="contact-panel__title">Realize seu Orçamento</h4>
                                            <p class="contact-panel__desc mb-40">Olá. Preencha os campos abaixo para
                                                realizarmos seu orçamento. Entraremos em contato através do seu e-mail,
                                                telefone ou whatsapp.
                                            </p>
                                        </div>
                                        <div class="col-sm-12 col-md-5 col-lg-5">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Seu nome" id="name"
                                                    name="name" required>
                                            </div>
                                        </div><!-- /.col-lg-6 -->
                                        <div class="col-sm-12 col-md-7 col-lg-7">
                                            <div class="form-group">
                                                <input type="email" class="form-control" placeholder="Seu E-mail"
                                                    id="email" name="email" required>
                                            </div>
                                        </div><!-- /.col-lg-6 -->
                                        <div class="col-sm-12 col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Seu telefone"
                                                    id="phone" name="phone" required>
                                            </div>
                                        </div><!-- /.col-lg-6 -->
                                        <div class="col-sm-12 col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Seu bairro"
                                                    id="distric" name="distric" required>
                                            </div>
                                        </div><!-- /.col-lg-6 -->
                                        <div class="col-sm-12 col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <select id="referencia" name="referencia" class="form-control"
                                                    style="display: none;">
                                                    <option>Como nos conheceu?</option>
                                                    <option>Encontrei no Google</option>
                                                    <option>Folhetos</option>
                                                    <option>Indicação de Amigos</option>
                                                    <option>Outra forma...</option>
                                                </select>
                                            </div>
                                        </div><!-- /.col-lg-6 -->
                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <textarea class="form-control" placeholder="Digite sua mensagem"
                                                    id="message" name="message" required></textarea>
                                            </div>
                                        </div><!-- /.col-lg-12 -->
                                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex flex-wrap align-items-center">
                                            <button type="submit" class="btn btn__secondary mr-40">
                                                <i class="fa fa-arrow-right"></i> <span>Enviar Orçamento</span>
                                            </button>
                                            <div class="form-group input-radio my-3">
                                                <span>Prazo estimado de resposta é de 1 dia</span>
                                            </div>
                                        </div><!-- /.col-lg-12 -->
                                    </div>
                                </form>
                                <div class="contact-result"></div>
                            </div>
                        </div><!-- /.contact__panel -->
                    </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.contact layout 2 -->

        <!-- ========================= 
         contact layout3
    =========================  -->
        <section class="contact-layout3 py-0" id="local">
            <div id="map-img"
                onclick="window.open('https://www.google.com/maps/place/Lavanderia+Zelo/@-23.6696218,-46.6595094,19z/data=!3m1!4b1!4m5!3m4!1s0x94ce45357a8ff599:0xba5a4506046e2368!8m2!3d-23.669623!4d-46.6589622?hl=pt-BR')">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-4 offset-lg-8">
                        <div class="contact-panel__info bg-white">
                            <div class="contact-panel__block">
                                <h5 class="contact-panel__block__title">Nossa Localização</h5>
                                <ul class="contact-panel__block__list list-unstyled">
                                    <li>R. Baquirivu, 454 - Cidade Ademar, São Paulo - SP</li>
                                </ul>
                            </div><!-- /.contact-panel__info__block -->
                            <div class="contact-panel__block">
                                <h5 class="contact-panel__block__title">Formas de Contato</h5>
                                <ul class="contact-panel__block__list list-unstyled">
                                    <li><a href="mailto:contato@lavanderiazelo.com.br"></a>E-mail:
                                        contato@lavanderiazelo.com.br</li>
                                    <li><a href="tel:+5511984683174"></a>Telefone: (11) 9.8468-3174</li>
                                </ul>
                            </div><!-- /.contact-panel__info__block -->
                            <div class="contact-panel__block">
                                <h5 class="contact-panel__block__title">Horário de Funcionamento</h5>
                                <ul class="contact-panel__block__list list-unstyled">
                                    <li>Segunda ~ Sexta</li>
                                    <li>08:00 AM às 19:00 PM</li>
                                    <li></li>
                                    <li>Sábado</li>
                                    <li>08:00 AM às 14:00 PM</li>
                                </ul>
                            </div><!-- /.contact-panel__info__block -->
                        </div>
                    </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.contact-layout3 -->

        <!-- ========================
      Footer
    ========================== -->
        <footer class="footer">
            <div class="footer-primary">
                <div class="container">
                    <div class="row d-flex justify-content-between">
                        <div class="col-sm-12 col-md-7 col-lg-7 col-xl-7 footer-widget footer-widget-about">
                            <h6 class="footer-widget-title">Sobre nós</h6>
                            <div class="footer-widget-content">
                                <p class="mb-20">Desde 1984 a Lavanderia Zelo proporciona a seus clientes um serviço de
                                    extrema qualidade na lavagem de sofás, tapetes, carpetes, cortinas, persianas, alem
                                    de impermeabilização e restauração de tapetes. <br> CNPJ: 36.262.435/0001-51</p>
                                <p class="mt-3"><b>Tecnologia:</b> <br> <a href="https://jpwtechdigital.com.br"
                                        target="_Blank"><img src="{{ asset('/galerias/jpw.webp?1') }}"
                                            alt="JPW Tech Digital" width="120" height="100%" style="max-width:110px;"></a> </p>
                            </div>
                        </div><!-- /.col-xl-4 -->
                        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 footer-widget footer-widget-contact">
                            <h6 class="footer-widget-title">Entre em Contato</h6>
                            <div class="footer-widget-content">
                                <p class="mb-20">Será um prazer atende-lo!</p>
                                <a class="contact-number contact-number-white d-flex align-items-center mb-20"
                                    href="tel:5511984683174">
                                    <i class="icon-phone"></i><span>(11) 9.8468-3174</span>
                                </a><!-- /.contact__numbr -->
                                <p class="mb-30">R. Baquirivu, 454 - Cidade Ademar, São Paulo - SP</p>
                            </div><!-- /.footer-widget-content -->
                        </div><!-- /.col-xl-4 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.footer-primary -->
            <div class="footer-copyrights">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                            <p class="mb-0"><a href="/">Lavanderia Zelo</a> - Todos os direitos reservados 1984 ~ 2020
                                &copy;</p>
                        </div><!-- /.col-lg-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.footer-copyrights-->
        </footer><!-- /.Footer -->
        <button id="scrollTopBtn"><i class="fas fa-long-arrow-alt-up"></i></button>

        <a href="https://api.whatsapp.com/send?phone=5511984683174&text=Ola%20gostaria%20de%20fazer%20um%20or%C3%A7amento.%20Te%20encontrei%20no%20site.%20Muito%20obrigado"
            id="whatsapp" class="btn-whatsapp" target="_Blank"><i class="fab fa-whatsapp"></i></a>

        <div class="search-popup">
            <i class="search-popup__close">&times;</i>
            <form class="search-popup__form">
                <input type="text" class="search-popup__form__input" placeholder="Type Words Then Enter">
                <button class="search-popup__btn"><i class="fas fa-search"></i></button>
            </form>
        </div><!-- /. search-popup -->

    </div><!-- /.wrapper -->

    <script src="{{ asset('/frontend/js/jquery-3.5.1.min.js?3') }}"></script>
    <script src="{{ asset('/frontend/js/plugins.js?2') }}"></script>
    <script src="{{ asset('/frontend/js/main.js?2') }}"></script>
    <script src="{{ asset('/frontend/js/jquery.mask.js') }}" type="text/javascript"></script>
    <script src="https://kit.fontawesome.com/04571ab3d2.js" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            // $('input[name="postcode"]').mask('00000-000');
            // $("input[name='postcode']").mask('00000-000', {reverse: true, placeholder: "00000-000"});
            // $('input[name="custom_field[account][1]"]').mask('000.000.000-00', {reverse: true});
            $('input[name="phone"]').mask("(99) 99999-9999");
            $('input[name="phone"]').on("blur", function() {
                var last = $(this).val().substr($(this).val().indexOf("-") + 1);

                if (last.length == 3) {
                    var move = $(this).val().substr($(this).val().indexOf("-") - 1, 1);
                    var lastfour = move + last;
                    var first = $(this).val().substr(0, 9);

                    $(this).val(first + '-' + lastfour);
                }
            });
        });

    </script>

    <script>
        // This is a functions that scrolls to #{blah}link
        function goToByScroll(id) {
            // Remove "link" from the ID
            id = id.replace("link", "");
            // Scroll
            $('html,body').animate({
                scrollTop: $("#" + id).offset().top
            }, 'slow');
        }

        $("nav.navbar .navbar-nav li a").click(function(e) {
            // Prevent a page reload when a link is pressed
            e.preventDefault();
            // Call the scroll function
            goToByScroll(this.id);
        });

        $('.gotoOrcamento').click(function(e) {
            e.preventDefault();
            goToByScroll('orcamentolink');
        });

    </script>
</body>

</html>
