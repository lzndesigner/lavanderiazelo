<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;
use DB;
use Carbon\Carbon;

class CampanhasController extends Controller
{
  // Dashboard
  public function cortina()
  {
    return view('campanhas.cortina');
  }
  public function persiana()
  {
    return view('campanhas.persiana');
  }
  public function sofa()
  {
    return view('campanhas.sofa');
  }
  public function tapete()
  {
    return view('campanhas.tapete');
  }
    
}
